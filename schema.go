package schema

import (
	"github.com/linkedin/goavro"
)

const schema = `{
	"type": "record",
	"name": "Message",
	"fields" : [
		{
		"name": "event",
		"type": "string"
		},
		{
		"name": "sender",
		"type": "string",
		"doc": "sender of the RCS message"
		},
		{
		"name": "recipient",
		"type": "string",
		"doc": "recipient of the SMS message"
		},
		{
		"name": "id",
		"type": "string",
		"doc": "correlation id"
		},
		{
		"name": "protocol",
		"type": "enum",
		"symbols":["smpp"],
		"doc": "protocol used by the pdu"
		},
		{
		"name": "pdu",
		"type": "bytes",
		"doc": "message pdu stored as an array of bytes for now"
		},
		{
		"name": "content",
		"type": "string",
		"doc": "message content to be adapted or passed thru"
		}
	]
}`

const Event = "event"
const Sender = "sender"
const Recipient = "recipient"
const Protocol = "protocol"
const ID = "id"
const PDU = "pdu"
const Content = "content"

type Message = map[string]interface{}

type MessageCodec struct {
	codec *goavro.Codec
}

func NewCodec() (MessageCodec, error) {
	c, err := goavro.NewCodec(schema)
	if err != nil {
		return MessageCodec{}, err
	}
	return MessageCodec{c}, nil
}

func (c *MessageCodec) Decode(avroBinary []byte) (Message, error) {
	x, _, err := c.codec.NativeFromBinary(avroBinary)
	if err != nil {
		return nil, err
	}
	return x.(Message), nil
}

func (c *MessageCodec) Encode(msg Message) ([]byte, error) {
	return c.codec.BinaryFromNative(nil, msg)
}
