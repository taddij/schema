package schema

import (
	"reflect"
	"testing"
)


func TestCreatingNewConnection(t *testing.T) {
	msg:= make(Message)
	msg[event] = "test"
	msg[sender] = "15142353585"
	msg[recipient] = "15148888888"
	msg[id] = "0"
	msg[protocol] = "smpp"
	msg[pdu] = make([]byte,20)
	msg[content] = "hello"

	c, err :=  NewCodec()

	if err != nil {
		t.Error("Not able to create a codec")
	}
	buf, err := c.Encode(msg)
	if err != nil {
		t.Error("Not able to encode")
	}
	msgDecoded,err := c.Decode(buf)

	if err != nil {
		t.Error("Not able to decode")
	}
	if msg[event] != msgDecoded[event] {
		t.Error("event not decoded")
	}
	if msg[sender] != msgDecoded[sender] {
		t.Error("sender not decoded")
	}
	if msg[recipient] != msgDecoded[recipient] {
		t.Error("recipient not decoded")
	}
	if msg[id] != msgDecoded[id] {
		t.Error("id not decoded")
	}
	if msg[protocol] != msgDecoded[protocol] {
		t.Error("protocol not decoded")
	}
	if !reflect.DeepEqual(msg[pdu], msgDecoded[pdu]) {
		t.Error("pdu not decoded")
	}
	if msg[content] != msgDecoded[content] {
		t.Error("event not decoded")
	}
}
